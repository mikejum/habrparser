﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Net;
using System.Text;
using System.IO;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        string header = "Дата Время Плюс Минус Всего Просмотры Закладки Комментарии Карма Рейтинг Подписчики";
        string startText = null;

        public Form1()
        {
            try
            {
                InitializeComponent();
                startText = header + "\n";

                //начальный текст   
                textBox.Text = startText;

                //восстановление настроек    
                address.Text = Properties.Settings.Default.Address;

                if (Properties.Settings.Default.Period > 0)
                    numericUpDown.Value = Properties.Settings.Default.Period;

                buttonStart.Focus();
            }

            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void button_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                if (!timer.Enabled)
                {
                    this.Text = "Сканирование...";
                    
                    numericUpDown.Enabled = false;
                    address.Enabled = false;
                    timer.Enabled = true;
                    buttonStart.Text = "Стоп";
                    buttonStart.Enabled = false;
                    buttonClear.Enabled = false;
                    label1.Enabled = false;
                    label2.Enabled = false;

                    timer.Interval = Convert.ToInt32(numericUpDown.Value) * 60000;
                    timer.Start();
                    buttonStart.Enabled = true;

                    parser();

                    this.Text = "Ожидание сканирования...";
                }
                else
                    activate();
                
                buttonStart.Focus();

                Cursor.Current = Cursors.Default;
            }

            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void activate()
        {
            timer.Enabled = false;
            timer.Stop();
            buttonStart.Text = "Старт";
            numericUpDown.Enabled = true;
            address.Enabled = true;
            buttonClear.Enabled = true;
            label1.Enabled = true;
            label2.Enabled = true;

            this.Text = "Динамика хабрапоста";
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                this.Text = "Сканирование...";

                parser();
                
                Cursor.Current = Cursors.Default;

                this.Text = "Ожидание сканирования...";
            }

            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        DateTime date1 = new DateTime();
        string connectBag = "//соединение с сайтом не установлено";

        //парсер страницы
        private void parser()
        {
            try
            {
                date1 = DateTime.Now;

                string url = address.Text.Trim();
                if (url == null || url == "")
                {
                    //останавливаем
                    activate();

                    MessageBox.Show("Не указан адрес.", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                string dateTime = date1.ToString();

                //соединяемся 
                string all = null;

                try
                {
                    all = GetUrl(url);
                }

                catch (Exception)
                {
                    textBox.Text += dateTime + " " + connectBag + "\n";
                    return;
                }

                findValue(all);

                int result = Convert.ToInt32(plus) - Convert.ToInt32(minus);
                double viewCount = convertViews(views);

                //добавляем в данные
                textBox.Text += dateTime + " " + Convert.ToInt32(plus) + " " + Convert.ToInt32(minus) + " " +
                    result + " " + viewCount + " " + bookmark + " " + comments + " " + karma +
                    " " + rating + " " + subscribers + "\n";

                //получаем текущее время 
                string min = DateTime.Now.Minute.ToString();
                if (min.Length == 1)
                    min = "0" + min;
                string time2 = DateTime.Now.Hour + ":" + min;

                //добавляем в графики 
                double plus1 = Convert.ToDouble(plus);
                double minus1 = Convert.ToDouble(minus);
                double total1 = Convert.ToDouble(plus) - Convert.ToDouble(minus);

                //Total
                chart1.Series["Total"].Points.AddXY(time2, total1);

                //Plus
                chart1.Series["Plus"].Points.AddXY(time2, plus1);  

                //Minus
                chart1.Series["Minus"].Points.AddXY(time2, minus1); 
                                            
                //вписываем остальное
                chart2.Series[0].Points.AddXY(time2, viewCount);
                chart3.Series[0].Points.AddXY(time2, rating);
                chart4.Series[0].Points.AddXY(time2, karma);
                chart5.Series[0].Points.AddXY(time2, bookmark);
                chart6.Series[0].Points.AddXY(time2, subscribers);
                chart7.Series[0].Points.AddXY(time2, comments);
            }

            catch (Exception exc)
            {
                //останавливаем
                activate();

                MessageBox.Show(exc.Message, "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        string plus = null;
        string minus = null;
        string views = null;
        string bookmark = null;
        string comments = null;
        string karma = null;
        string rating = null;
        string subscribers = null;

        //конвертация просмотров в число
        private double convertViews(string count)
        {
            double result = 0;

            try
            {

                if (count.Contains("k"))
                {
                    string number = count.Substring(0, count.Length - 1);
                    result = Convert.ToDouble(number) * 1000;
                }
                else
                    result = Convert.ToInt32(count);
            }

            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            return result;
        }

        //соединение с сайтом
        public string GetUrl(string address)
        {
            WebClient client = new WebClient();

            try
            {
                client.Credentials = CredentialCache.DefaultNetworkCredentials;
            }

            catch (Exception)
            {
            }

            return client.DownloadString(address);
        }

        //поиск на сайте показателей
        private void findValue(string all)
        {
            try
            {
                Encoding source = Encoding.Unicode;
                Encoding win1251 = Encoding.GetEncoding(1251);
                byte[] win1251Bytes = Encoding.Convert(source, win1251, source.GetBytes(all));                
                all = Encoding.UTF8.GetString(win1251Bytes);

                string find = "voting-wjt__counter";
                string find2 = "post-stats__views-count";
                string find3 = "js-favs_count";
                string find4 = "comments-section__head-counter";
                string find5 = "stacked-counter__value_green";
                string find6 = "stacked-counter__value_magenta";
                string find7 = "stacked-counter__value_blue";
                
                string[] stringSeparators = new string[] { "<div" };
                string[] words = all.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

                List<string> wordsList = new List<string>(words);
                bool stop1 = false;
                bool stop2 = false;
                bool stop3 = false;
                bool stop4 = false;
                bool stop5 = false;
                bool stop6 = false;
                bool stop7 = false;

                for (int i = 0; i < wordsList.Count; i++)
                {
                    string s = wordsList[i];

                    //ищем плюс-минус
                    if (!stop1)
                    {
                        int index = s.IndexOf(find);

                        if (index != -1)
                        {
                            string string1 = "&uarr;";
                            string string2 = "&darr;";

                            int indexP = s.IndexOf(string1);
                            int index2P = s.IndexOf(" ", indexP);
                            int lengthP = index2P - indexP;
                            plus = s.Substring(indexP + string1.Length, lengthP - string1.Length).Trim();
                            if (plus.Contains(" "))
                                plus = plus.Replace(" ", "");

                            int indexM = s.IndexOf(string2);
                            int index2M = s.IndexOf("\"", indexM);
                            int lengthM = index2M - indexM;

                            if (lengthM > 0)
                            {
                                minus = s.Substring(indexM + string2.Length, lengthM - string2.Length).Trim();
                                if (minus.Contains(" "))
                                    minus = minus.Replace(" ", "");
                            }

                            stop1 = true;
                        }
                    }

                    //ищем количество просмотров
                    if (!stop2)
                    {
                        int indexC = s.IndexOf(find2);

                        if (indexC != -1)
                        {
                            int index1C = s.IndexOf(">", indexC) + 1;
                            int index2C = s.IndexOf("<", indexC);
                            int lengthC = index2C - index1C;

                            if (lengthC > 0)
                            {
                                views = s.Substring(index1C, lengthC).Trim();
                                if (views.Contains(" "))
                                    views = views.Replace(" ", "");
                                if (views == null || views == "")
                                    views = "0";
                            }

                            stop2 = true;
                        }
                    }

                    //ищем закладки
                    if (!stop3)
                    {
                        int indexB = s.IndexOf(find3);

                        if (indexB != -1)
                        {
                            int index1B = s.IndexOf(">", indexB) + 1;
                            int index2B = s.IndexOf("<", indexB);
                            int lengthB = index2B - index1B;

                            if (lengthB > 0)
                            {
                                bookmark = s.Substring(index1B, lengthB).Trim();
                                if (bookmark.Contains(" "))
                                    bookmark = bookmark.Replace(" ", "");
                                if (bookmark == null || bookmark == "")
                                    bookmark = "0";
                            }

                            stop3 = true;
                        }
                    }

                    //ищем комментарии
                    if (!stop4)
                    {
                        int indexB = s.IndexOf(find4);

                        if (indexB != -1)
                        {
                            int index1B = s.IndexOf(">", indexB) + 1;
                            int index2B = s.IndexOf("<", indexB);
                            int lengthB = index2B - index1B;

                            if (lengthB > 0)
                            {
                                comments = s.Substring(index1B, lengthB).Trim();
                                if (comments.Contains(" "))
                                    comments = comments.Replace(" ", "");
                                if (comments == null || comments == "")
                                    comments = "0";
                            }

                            stop4 = true;
                        }
                    }

                    //ищем карму
                    if (!stop5)
                    {
                        int indexB = s.IndexOf(find5);

                        if (indexB != -1)
                        {
                            int index1B = s.IndexOf(">", indexB) + 1;
                            int index2B = s.IndexOf("<", indexB);
                            int lengthB = index2B - index1B;

                            if (lengthB > 0)
                            {
                                karma = s.Substring(index1B, lengthB).Trim();
                                if (karma.Contains(" "))
                                    karma = karma.Replace(" ", "");

                                if (karma == null || karma == "")
                                    karma = "0";

                                karma = karma.Replace(",", ".");
                            }

                            stop5 = true;
                        }
                    }

                    //ищем рейтинг
                    if (!stop6)
                    {
                        int indexB = s.IndexOf(find6);

                        if (indexB != -1)
                        {
                            int index1B = s.IndexOf(">", indexB) + 1;
                            int index2B = s.IndexOf("<", indexB);
                            int lengthB = index2B - index1B;

                            if (lengthB > 0)
                            {
                                rating = s.Substring(index1B, lengthB).Trim();
                                if (rating.Contains(" "))
                                    rating = rating.Replace(" ", "");
                                if (rating == null || rating == "")
                                    rating = "0";

                                rating = rating.Replace(",", ".");
                            }

                            stop6 = true;
                        }
                    }

                    //ищем подписчиков
                    if (!stop7)
                    {
                        int indexB = s.IndexOf(find7);

                        if (indexB != -1)
                        {
                            int index1B = s.IndexOf(">", indexB) + 1;
                            int index2B = s.IndexOf("<", indexB);
                            int lengthB = index2B - index1B;

                            if (lengthB > 0)
                            {
                                subscribers = s.Substring(index1B, lengthB).Trim();
                                if (subscribers.Contains(" "))
                                    subscribers = subscribers.Replace(" ", "");
                                if (subscribers == null || subscribers == "")
                                    subscribers = "0";
                            }

                            stop7 = true;
                        }
                    }

                    if (stop1 && stop2 && stop3 && stop4 && stop5 && stop6 && stop7)
                        break;
                }
            }

            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        //сохранение настроек    
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.Address = address.Text;
            Properties.Settings.Default.Period = Convert.ToInt32(numericUpDown.Value);
            Properties.Settings.Default.Save();
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonCopy_Click(object sender, EventArgs e)
        {
            string saveText = address.Text + "\n" + textBox.Text;
            Clipboard.SetText(saveText);
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            //восстанавливаем первоначальное положение   
            textBox.Text = startText;

            chart1.Series["Plus"].Points.Clear();
            chart1.Series["Minus"].Points.Clear();
            chart1.Series["Total"].Points.Clear();

            chart2.Series[0].Points.Clear();
            chart3.Series[0].Points.Clear();
            chart4.Series[0].Points.Clear();
            chart5.Series[0].Points.Clear();
            chart6.Series[0].Points.Clear();
            chart7.Series[0].Points.Clear();
        }

        private void buttonAbout_Click(object sender, EventArgs e)
        {
            var form = new FormAbout();
            form.ShowDialog();
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedIndex == 0)
                buttonCopy.Enabled = true;
            else
                buttonCopy.Enabled = false;
        }

        private void address_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                buttonStart.PerformClick();
            }
        }

        //экспорт
        private void buttonExport_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                                
                saveFileDialog1.Filter = "текстовые файлы" + " (*.txt)|*.txt";
                saveFileDialog1.RestoreDirectory = true;

                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    // получаем выбранный файл
                    string filename = saveFileDialog1.FileName;
                    string saveText = address.Text + "\n" + textBox.Text;

                    // сохраняем текст в файл
                    File.WriteAllText(filename, saveText, Encoding.GetEncoding(1251));
                    MessageBox.Show("Файл сохранен", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                Cursor.Current = Cursors.Default;
            }

            catch (Exception exc)
            {
                Cursor.Current = Cursors.Default;

                MessageBox.Show(exc.Message, "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        //импорт
        private void buttonImport_Click(object sender, EventArgs e)
        {
            try
            {
                Stream myStream;
                OpenFileDialog saveFileDialog1 = new OpenFileDialog();

                Cursor.Current = Cursors.WaitCursor;

                saveFileDialog1.Filter = "текстовые файлы" + " (*.txt)|*.txt";
                saveFileDialog1.FilterIndex = 1;
                saveFileDialog1.RestoreDirectory = true;

                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    if ((myStream = saveFileDialog1.OpenFile()) != null)
                    {
                        //если все нормально, продолжаем 
                        string extension = Path.GetExtension(saveFileDialog1.FileName);
                        
                        if (extension == ".txt")
                        {
                            string[] lines = File.ReadAllLines(saveFileDialog1.FileName, Encoding.Default);
                            import(lines);
                        }
                    }
                }

                Cursor.Current = Cursors.Default;
            }

            catch (Exception exc)
            {
                Cursor.Current = Cursors.Default;
                MessageBox.Show(exc.Message, "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        //обрабатываем импортированное
        private void import(string[] words)
        {
            try
            {
                List<string> wordsList = new List<string>(words);

                if (wordsList.Count == 0)
                {
                    MessageBox.Show("Файл пустой.", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                //очищаем
                buttonClear.PerformClick();

                //обрабатываем
                for (int i = 1; i < wordsList.Count; i++)//со второй строки
                {
                    string line = wordsList[i];

                    if (line.Contains(connectBag))
                        continue;

                    if (i == 1 && line != header)
                    {
                        MessageBox.Show("Ошибка в формате.", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    else if (i > 1)
                    {
                        string[] stringSeparators2 = new string[] { " " };
                        string[] words2 = line.Split(stringSeparators2, StringSplitOptions.RemoveEmptyEntries);
                        List<string> wordsList2 = new List<string>(words2);

                        double plus0 = 0;
                        double minus0 = 0;
                        string time2 = "";

                        for (int s = 1; s < wordsList2.Count; s++)//дату не читаем
                        {
                            string value = wordsList2[s];

                            if (s == 1)
                                time2 = value.Substring(0, value.Length - 3);
                            else if (s == 2)
                            {
                                chart1.Series["Plus"].Points.AddXY(time2, value);
                                plus0 = Convert.ToDouble(value);
                            }
                            else if (s == 3)
                            {
                                chart1.Series["Minus"].Points.AddXY(time2, value);
                                minus0 = Convert.ToDouble(value);
                            }
                            else if (s == 4)
                                chart1.Series["Total"].Points.AddXY(time2, plus0 - minus0);
                            else if (s == 5)
                                chart2.Series[0].Points.AddXY(time2, value);
                            else if (s == 6)
                                chart5.Series[0].Points.AddXY(time2, value);
                            else if (s == 7)
                                chart7.Series[0].Points.AddXY(time2, value);
                            else if (s == 8)
                                chart4.Series[0].Points.AddXY(time2, value);
                            else if (s == 9)
                                chart3.Series[0].Points.AddXY(time2, value);
                            else if (s == 10)
                                chart6.Series[0].Points.AddXY(time2, value);
                        }
                    }
                }
                
                address.Text = wordsList[0];
                wordsList.RemoveAt(0);
                textBox.Text = String.Join(Environment.NewLine, wordsList);

                MessageBox.Show("Импорт успешно завершен.", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            catch (Exception)
            {
                Cursor.Current = Cursors.Default;
                MessageBox.Show("Ошибка при форматировании данных.", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }






    }
}
